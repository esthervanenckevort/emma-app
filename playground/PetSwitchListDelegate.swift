//
//  PetSwitchListDelegate.swift
//  Emma
//
//  Created by David van Enckevort on 31-05-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import Foundation
import UIKit

protocol PetSwitchListDelegate {
    func presentPetSwitchList(viewController: UIViewController, listOfPets pets: [Pet], completion: ((Pet) -> Void)?)
    func setPetSwitchListButtonImage(button: UIBarButtonItem, activePet pet: Pet?)
}

extension PetSwitchListDelegate {
    func presentPetSwitchList(viewController: UIViewController, listOfPets pets: [Pet], completion: ((Pet) -> Void)?) {
        let actionsheet = UIAlertController(title: I18N.switchPetTitle,
                                            message: I18N.switchPetMessage,
                                            preferredStyle: .ActionSheet)
        for pet in pets {
            let action = UIAlertAction(title: pet.name, style: .Default) {
                UIAlertAction in
                completion?(pet)
            }
            actionsheet.addAction(action)
        }
        let action = UIAlertAction(title: I18N.cancelButtonText,
                                   style: .Cancel, handler: nil)
        actionsheet.addAction(action)
        viewController.presentViewController(actionsheet, animated: true, completion: nil)
    }

    func setPetSwitchListButtonImage(barButton: UIBarButtonItem, activePet pet: Pet?) {
        guard let button = barButton.customView as? UIButton else {
            abort()
        }
        var image : UIImage?
        if let petImage = pet?.image {
            image = UIImage(data: petImage)
        } else {
            image = Resources.catImage
        }
        let size = CGSize(width: 22, height: 22)
        let rect = CGRectMake(0, 0, 22, 22)
        UIGraphicsBeginImageContext(size)
        image?.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        button.setImage(newImage, forState: .Normal)
        button.setImage(newImage, forState: .Highlighted)
    }
    
}
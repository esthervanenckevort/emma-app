//
//  Observation.swift
//  playground
//
//  Created by David van Enckevort on 26-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import Foundation
import CoreData

@objc(Observation)
class Observation: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    static let entityName = "Observation"
    enum Categories: String {
        case Behaviour = "Behaviour"
        case Development = "Development"
        case Nutrition = "Nutrition"
        case Reproduction = "Reproduction"
    }

    enum Observation: String {
        case Aggression = "Aggression"
        case Play = "Play"
        case Eating = "Eating"
        case Drinking = "Drinking"
    }

    static let categories = [
        Categories.Behaviour,
        Categories.Development,
        Categories.Nutrition,
        Categories.Reproduction
    ]
    static let observations = [
        Categories.Behaviour: [
            Observation.Aggression,
            Observation.Play
        ],
        Categories.Development: [ ],
        Categories.Nutrition: [ ],
        Categories.Reproduction: [ ]
    ]
}

//
//  SecondViewController.swift
//  playground
//
//  Created by David van Enckevort on 19-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit

class PetIDViewController: UIViewController, PetActor, UITextFieldDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate, PetSwitchListDelegate {

    @IBOutlet weak var navBar: UINavigationItem!
    @IBOutlet weak var selectPet: UIBarButtonItem!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var chip: UITextField!
    @IBOutlet weak var name: UITextField!
    var kbHeight: CGFloat?
    var petDataSource: PetDataSource?

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(PetIDViewController.keyboardWillShow(_:)),
                                                         name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(PetIDViewController.keyboardWillHide(_:)),
                                                         name: UIKeyboardWillHideNotification, object: nil)
        updateUI()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    func updateUI() {
        title = pet?.name
        name.text = pet?.name
        chip.text = pet?.chip
        title = pet?.name
        if let image = pet?.image {
            picture.image = UIImage(data: image)
        } else {
            picture.image = Resources.catImage
        }
        setPetSwitchListButtonImage(selectPet, activePet: pet)
    }


    @IBAction func selectPet(sender: UIButton) {
        let pets = Pet.listPets(inManagedObjectContext: context)
        presentPetSwitchList(self, listOfPets: pets) {
            pet in
            self.petDataSource?.pet = pet
            dispatch_async(dispatch_get_main_queue()) {
                self.updateUI()
            }
        }
    }

    // MARK: Notifications
    func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]
            as? NSValue)?.CGRectValue() else { return }
        if (kbHeight == nil) {
            kbHeight = keyboardSize.height

            moveView(true)
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        if kbHeight != nil {
            moveView(false)
            kbHeight = nil
        }
    }

    // MARK: Manage view position
    func moveView(up: Bool) {
        if let height = kbHeight {
            let movement = (up ? -height : height)
            dispatch_async(dispatch_get_main_queue()) {
                UIView.animateWithDuration(0.3, animations: {
                    self.view.frame = CGRectOffset(self.view.frame, 0, movement)
                })
            }
        }
    }
    // MARK: Image picker
    @IBAction func showImagePicker(sender: UITapGestureRecognizer) {
        let location = sender.locationInView(view)
        if picture.pointInside(location, withEvent: nil) {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .PhotoLibrary
            imagePicker.delegate = self
            presentViewController(imagePicker, animated: true, completion: nil)
        }
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            petDataSource?.pet?.image = UIImagePNGRepresentation(image)
            _ = try? context.save()
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}


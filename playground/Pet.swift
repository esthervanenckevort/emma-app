//
//  Pet.swift
//  playground
//
//  Created by David van Enckevort on 20-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import Foundation
import CoreData


class Pet: NSManagedObject {

    static let entityName = "Pet"

// Insert code here to add functionality to your managed object subclass

    class func selectPet(pet: Pet) {
        let uri = pet.objectID.URIRepresentation()
        NSUserDefaults.standardUserDefaults().setURL(uri, forKey: "Pet")
    }

    class func listPets(inManagedObjectContext context: NSManagedObjectContext) -> [Pet] {
        var pets: [Pet]
        do {
            let request = NSFetchRequest(entityName: Pet.entityName)
            request.returnsObjectsAsFaults = false
            request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
            pets = try context.executeFetchRequest(request) as! [Pet]
        } catch {
            print("Failed to get list of pets.")
            pets = [Pet]()
        }
        return pets
    }

    class func getSelectedPet(inManagedObjectContext context: NSManagedObjectContext) -> Pet {
        if let uri = NSUserDefaults.standardUserDefaults().URLForKey("Pet") {
            if let objectID = context.persistentStoreCoordinator?.managedObjectIDForURIRepresentation(uri) {
                return context.objectWithID(objectID) as! Pet
            }
        }
        return listPets(inManagedObjectContext: context).first ?? newPet(I18N.placeholderPetName, inManagedObjectContext: context)
    }

    class func getPet(name: String, inManagedObjectContext context: NSManagedObjectContext) -> Pet {
        var pets = [Pet]()
        do {
            let request = NSFetchRequest(entityName: Pet.entityName)
            request.predicate = NSPredicate(format: "name = %@", name)
            request.returnsObjectsAsFaults = false
            pets = try context.executeFetchRequest(request) as! [Pet]
        } catch {
            print("Failed to fetch pet with name \(name)")
        }
        return pets.first ?? newPet(name, inManagedObjectContext: context)
    }

    class func newPet(name: String, inManagedObjectContext context: NSManagedObjectContext) -> Pet {
        let pet = NSEntityDescription.insertNewObjectForEntityForName(Pet.entityName,
                                                                      inManagedObjectContext: context) as! Pet
        pet.name = name
        return pet
    }

    class func deletePet(pet: Pet) -> Bool {
        if let context = pet.managedObjectContext {
            do {
                context.deleteObject(pet)
                try context.save()
                return true
            } catch {
                print("Failed to delete \(pet.name), rolling back.")
                context.rollback()
            }
        }
        return false
    }

}

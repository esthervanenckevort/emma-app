//
//  Observation+CoreDataProperties.swift
//  playground
//
//  Created by David van Enckevort on 26-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Observation {

    @NSManaged var category: String?
    @NSManaged var timestamp: NSDate?
    @NSManaged var value: NSData?
    @NSManaged var observation: String?
    @NSManaged var pet: Pet?

}

//
//  MeasurementsViewController.swift
//  playground
//
//  Created by David van Enckevort on 21-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit

class MeasurementsViewController: UITableViewController, PetActor, PetSwitchListDelegate {

    var petDataSource: PetDataSource?
    
    @IBOutlet var table: UITableView!

    @IBOutlet weak var selectPet: UIBarButtonItem!
    override func viewWillAppear(animated: Bool) {
        updateUI()
    }

    func updateUI() {
        title = petDataSource?.pet?.name
        table.reloadData()
        setPetSwitchListButtonImage(selectPet, activePet: pet)
    }

    // MARK: UITableView
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("measurement") else {
            fatalError("Can't dequeue reusable cell")
        }
        let category = Observation.categories[indexPath.row]
        cell.textLabel?.text = category.rawValue
        return cell
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Observation.observations.count
    }

    // MARK: Actions
    @IBAction func selectPet(sender: UIButton) {
        let pets = Pet.listPets(inManagedObjectContext: context)
        presentPetSwitchList(self, listOfPets: pets) {
            pet in
            self.petDataSource?.pet = pet
            dispatch_async(dispatch_get_main_queue()) {
                self.updateUI()
            }
        }
    }
}

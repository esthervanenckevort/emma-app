//
//  UIViewExtensions.swift
//  playground
//
//  Created by David van Enckevort on 20-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit
import CoreData
extension UIViewController {
    var contentViewController: UIViewController? {
        get {
            if let vc = self as? UINavigationController {
                return vc.visibleViewController
            }
            return self
        }
    }

    func displayAlert(message: String, title: String) {
        let alert = UIAlertController(title: title, message: message,
            preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: Dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override public func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}
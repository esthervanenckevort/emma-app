//
//  PetDataSource.swift
//  playground
//
//  Created by David van Enckevort on 19-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit
import CoreData

protocol PetDataSource {
    var pet: Pet? { get set }
}

extension PetDataSource {
    // Expose the Core Data context
    var context: NSManagedObjectContext {
        get {
            return (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        }
    }
}
//
//  resources.swift
//  playground
//
//  Created by David van Enckevort on 23-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit

struct Resources {
    static let catImage = UIImage(named: "cat")
}
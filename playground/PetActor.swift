//
//  PetActor.swift
//  playground
//
//  Created by David van Enckevort on 20-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit
import CoreData

protocol PetActor: class {
    var petDataSource: PetDataSource? { get set }
}


extension PetActor {
    // Expose the Core Data context
    var context: NSManagedObjectContext {
        get {
            return (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        }
    }

    // Convenience access to the DataSource's pet model
    var pet : Pet? {
        get {
            return petDataSource?.pet
        }
        set {
            petDataSource?.pet = newValue
        }
    }
}

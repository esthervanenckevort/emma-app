//
//  i18n.swift
//  playground
//
//  Created by David van Enckevort on 23-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import Foundation

struct I18N {
    // Common buttons
    static let okButtonText = NSLocalizedString("OK",
        comment: "OK button text")
    static let cancelButtonText = NSLocalizedString("Cancel",
        comment: "Cancel button text")
    // Common titles
    static let errorTitle = NSLocalizedString("Error",
        comment: "Title of an error dialog box")
    // Switch pet action sheet
    static let switchPetTitle = NSLocalizedString("Switch pet",
        comment: "Title of the switch pet action sheet")
    static let switchPetMessage = NSLocalizedString("Select your pet",
        comment: "Message of the switch pet action sheet")
    // Pet's name dialog
    static let petNameMessage = NSLocalizedString("Please enter your pet's name",
        comment: "Message text asking for the pet's name")
    static let petNameTitle = NSLocalizedString("Pet's name",
        comment: "Title of the Pet's name dialog")
    // Error message
    static let failAddPetErrorMessage = NSLocalizedString("Failed to add pet",
        comment: "Error message for CoreData errors while adding a pet")
    static let coreDateSaveErrorMessage = NSLocalizedString("Failed to save data",
        comment: "Error message for CoreData errors while saving data")
    static let failDeletePetErrorMessage = NSLocalizedString("Failed to delete pet",
        comment: "Error message for CoreData errors while deleting a pet instance")
    static let internalStateErrorMessage = NSLocalizedString("Internal state inconsistency",
        comment: "Error message when the internal state of a viewcontroler is inconsistent")
    // Pet placeholder texts
    static let placeholderPetName = NSLocalizedString("My new pet's name",
        comment: "Placeholder text for a pet's name")
    // Categories of observations
    static let behaviourCategoryTitle = NSLocalizedString("Behaviour",
        comment: "Behaviour observeration category title")
    static let developmentCategoryTitle = NSLocalizedString("Development",
        comment: "Development observation category title")
    static let reproductiveCategoryTitle = NSLocalizedString("Reproductive",
        comment: "Reproductive behaviour observeration category title")
    static let nutritionCategoryTitle = NSLocalizedString("Nutrition",
        comment: "Nutrition observation category title")
    // Behaviour observations
    static let behaviourAggressionTitle = NSLocalizedString("Aggression",
        comment: "Aggression observation title")
    static let behaviourPlayTitle = NSLocalizedString("Play",
        comment: "Play observation title")
}
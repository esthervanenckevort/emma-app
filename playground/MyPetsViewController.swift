//
//  FirstViewController.swift
//  playground
//
//  Created by David van Enckevort on 19-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit

class MyPetsViewController: UIViewController, PetActor, UITextFieldDelegate, UINavigationBarDelegate, UITableViewDataSource, UITableViewDelegate {

    var petDataSource: PetDataSource?

    var pets: [Pet]? {
        get {
            return Pet.listPets(inManagedObjectContext: context)
        }
    }

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        title = petDataSource?.pet?.name
        table.reloadData()
    }

    // MARK: - Button actions
    @IBAction func addPet(sender: UIBarButtonItem) {
        let popup = UIAlertController(title: I18N.petNameTitle,
                                      message: I18N.petNameMessage, preferredStyle: .Alert)
        popup.addTextFieldWithConfigurationHandler() { (textField: UITextField!) in
            textField.delegate = self
        }
        let action = UIAlertAction(title: I18N.okButtonText, style: .Default) {
            (UIAlertAction) in
            if let textField = popup.textFields?[0] {
                if let name = textField.text {
                    self.context.performBlock {
                        self.petDataSource?.pet = Pet.getPet(name, inManagedObjectContext: self.context)
                        dispatch_async(dispatch_get_main_queue()) {
                            self.table.reloadData()
                        }
                    }
                }
            }
        }
        popup.addAction(action)
        presentViewController(popup, animated: true, completion: nil)
    }

    // MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("pet") {
            if let pet = pets?[indexPath.row] {
                cell.textLabel?.text = pet.name
                cell.detailTextLabel?.text = pet.chip
                if let data = pet.image {
                    cell.imageView?.image = UIImage(data: data)
                } else {
                    cell.imageView?.image = Resources.catImage
                }
                return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets?.count ?? 0
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        petDataSource?.pet = (pets?[indexPath.row])!
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        guard let pet = pets?[indexPath.row] else {
            fatalError(I18N.internalStateErrorMessage)
        }
        if editingStyle == .Delete {
            if (Pet.deletePet(pet) == true) {
                table.reloadData()
            } else {
                displayAlert(I18N.failDeletePetErrorMessage, title: I18N.errorTitle)
            }
        }
    }
}


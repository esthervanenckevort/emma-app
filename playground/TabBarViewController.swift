//
//  TabBarViewController.swift
//  playground
//
//  Created by David van Enckevort on 19-01-16.
//  Copyright © 2016 David van Enckevort. All rights reserved.
//

import UIKit
import CoreData

class TabBarViewController: UITabBarController, PetDataSource {

    var pet: Pet?

    override func viewDidLoad() {
        super.viewDidLoad()
        context.performBlockAndWait {
            self.pet = Pet.getSelectedPet(inManagedObjectContext: self.context)
        }

        for vc in viewControllers! {
            if let petActor = vc.contentViewController as? PetActor {
                petActor.petDataSource = self
            }
        }
    }
}
